
import os
import xlrd

from hero_startup.settings import BASE_DIR
from django.contrib.auth.models import User

from core.utils import parsing_dates
from courses.models import Course, CoursesRegistration


def registration_parser():
    students_file = os.path.join(BASE_DIR, 'parsers/files/Inscripcion_curso-2021-03-09.xls')
    wb = xlrd.open_workbook(students_file)
    sh = wb.sheet_by_index(0)

    for i in range(1, sh.nrows):

        user = User.objects.get(id=sh.row_values(i)[1])
        course = Course.objects.get(id=sh.row_values(i)[2])
        start = parsing_dates(sh.row_values(i)[3])
        end = parsing_dates(sh.row_values(i)[4])

        print(f'added {i} - {user} {user.id}')

        CoursesRegistration.objects.create(
            id=sh.row_values(i)[0],
            student=user,
            course=course,
            registration_date=start,
            expiration_date=end,
            status=sh.row_values(i)[5]
        )
