import os
import xlrd

from hero_startup.settings import BASE_DIR

from core.utils import parsing_dates
from courses.models import CoursesRegistration
from topics.models import Topic, VideoAnalytics


# def getting_course(course_id):
#     """Some of the courses in the document doesn't exists in the DB

#     Args:
#         course_id (str): The course ID

#     Returns:
#         Obj: The course object or None
#     """
#     try:
#         return Course.objects.get(id=course_id)
#     except:
#         return None


def analytic_parser():
    analytic_file = os.path.join(BASE_DIR, 'parsers/files/AnaliticaVideo-2021-03-09.xls')
    wb = xlrd.open_workbook(analytic_file)
    sh = wb.sheet_by_index(0)

    for i in range(1, sh.nrows):
        # course = getting_course(sh.row_values(i)[1])

        register = CoursesRegistration.objects.get(id=sh.row_values(i)[1])
        topic = Topic.objects.get(id=sh.row_values(i)[4])

        print(f'adding line {i} - course_id {sh.row_values(i)[1]}')

        VideoAnalytics.objects.create(
            id=sh.row_values(i)[0],
            course_register=register,
            created_at=parsing_dates(sh.row_values(i)[2]),
            updated_at=parsing_dates(sh.row_values(i)[3]),
            topic=topic,
            finished=sh.row_values(i)[5],
            played_quantity=sh.row_values(i)[6],
            full_screen=sh.row_values(i)[7]
        )
        # else:
        #     pass

    print(f'added {VideoAnalytics.objects.all().count()} registers')
