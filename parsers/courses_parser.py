import os
import xlrd

from hero_startup.settings import BASE_DIR

from core.utils import parsing_dates
from courses.models import Course


def courses_parser():
    courses_file = os.path.join(BASE_DIR, 'parsers/files/Curso-2021-03-09.xls')
    wb = xlrd.open_workbook(courses_file)
    sh = wb.sheet_by_index(0)

    for i in range(1, sh.nrows):

        created = parsing_dates(sh.row_values(i)[5])
        start = parsing_dates(sh.row_values(i)[11])
        end = parsing_dates(sh.row_values(i)[12])

        print(f'adding {i} - {sh.row_values(i)[0]}')

        Course.objects.create(
            id = sh.row_values(i)[0],
            name = sh.row_values(i)[1],
            short_name = sh.row_values(i)[2],
            slug = sh.row_values(i)[3],
            duration = sh.row_values(i)[4],
            created_at = created,
            status = sh.row_values(i)[6],
            free = sh.row_values(i)[7],
            currency = sh.row_values(i)[8],
            price = sh.row_values(i)[9],
            installments = sh.row_values(i)[10],
            start_date = start,
            end_date = end
        )
    
    print(f'Added {Course.objects.all().count()}')
