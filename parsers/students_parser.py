import os
import re
import xlrd

from hero_startup.settings import BASE_DIR
from django.contrib.auth.models import User, Group


def students_parser():
    students_file = os.path.join(BASE_DIR, 'parsers/files/Usuario-2021-03-09.xls')
    wb = xlrd.open_workbook(students_file)
    sh = wb.sheet_by_index(0)
    
    for i in range(1, sh.nrows):
        user_id = sh.row_values(i)[0]
        groups = sh.row_values(i)[1]
        username = sh.row_values(i)[2]

        user = User.objects.create(
            id=int(user_id),
            username=username,
        )

        print(f'usuario {user_id} agregado')

        if re.match('1', groups):   
            user.groups.add(1)
            print(f'usuario {user_id} agregado a grupo')
