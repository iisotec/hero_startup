import os
import xlrd

from hero_startup.settings import BASE_DIR

from core.utils import parsing_dates
from topics.models import Topic


def topic_parser():
    topic_file = os.path.join(BASE_DIR, 'parsers/files/Tema-2021-03-09.xls')
    wb = xlrd.open_workbook(topic_file)
    sh = wb.sheet_by_index(0)

    for i in range(1, sh.nrows):
        print(f'adding {i} - {sh.row_values(i)[1]} - {sh.row_values(i)[0]}')
        Topic.objects.create(
            id=sh.row_values(i)[0],
            title=sh.row_values(i)[1],
            short_title=sh.row_values(i)[2],
            slug=sh.row_values(i)[3],
            document_type=sh.row_values(i)[4],
            created_at=parsing_dates(sh.row_values(i)[5]),
            updated_at=parsing_dates(sh.row_values(i)[6]),
            status=sh.row_values(i)[7],
            free=sh.row_values(i)[8],
            order=sh.row_values(i)[9]
        )
    
    print(f'Added {Topic.objects.all().count()} topics')
