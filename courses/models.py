from django.contrib.auth.models import User
from django.db import models


class Course(models.Model):
    name = models.CharField(max_length=255, verbose_name='Nombre')
    short_name = models.CharField(max_length=255, verbose_name='Nombre corto')
    slug = models.SlugField()
    duration = models.IntegerField(verbose_name='Duración')
    created_at = models.DateTimeField(blank=True, null=True, verbose_name='Fecha de creación')
    status = models.BooleanField(verbose_name='Estado')
    free = models.BooleanField(verbose_name='Es Gratis')
    currency = models.CharField(max_length=5, verbose_name='Moneda')
    price = models.DecimalField(max_digits=9, decimal_places=2, verbose_name='Costo')
    installments = models.IntegerField(verbose_name='Cuotas a pagar')
    start_date = models.DateTimeField(blank=True, null=True, verbose_name='Fecha de inicio')
    end_date = models.DateTimeField(blank=True, null=True, verbose_name='Fecha de finalización')

    class Meta:
        verbose_name = 'Curso'
        verbose_name_plural = 'Cursos'

    @property
    def total_students(self):
        return self.course_register.all().count()
    
    def __str__(self):
        return self.name


class CoursesRegistration(models.Model):
    student = models.ForeignKey(User, related_name='course_student', on_delete=models.CASCADE, verbose_name='Estudiante')
    course = models.ForeignKey(Course, related_name='course_register', on_delete=models.CASCADE, verbose_name='Curso')
    registration_date = models.DateTimeField(blank=True, null=True,  verbose_name='Fecha de registro')
    expiration_date = models.DateTimeField(blank=True, null=True, verbose_name='Fecha de vencimiento')
    status = models.BooleanField(verbose_name='Estado')

    class Meta:
        verbose_name = 'Inscripción'
        verbose_name_plural = 'Inscripciones'
    
    def __str__(self):
        return f'{self.student} - {self.course}'
