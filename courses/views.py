from django.shortcuts import render
from django.views.generic import DetailView, TemplateView

from .models import CoursesRegistration


class RegistrationDetail(TemplateView):
    template_name = 'courses/courses_list_students.html'

    def get_context_data(self, **kwargs):
        regs = CoursesRegistration.objects.filter(course=kwargs['pk'])

        context = super(RegistrationDetail, self).get_context_data()
        context['students'] = [reg.student for reg in regs]
        context['course'] = kwargs['pk']
        context['title'] = 'Estudiantes registrados'
        context['subtitle'] = regs[0].course.name
        return context
