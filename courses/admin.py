from django.contrib import admin

from .models import Course, CoursesRegistration


class CourseAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'duration', 'status', 'free', 'currency',
        'price', 'installments', 'start_date', 'end_date'
    )
    readonly_fields = ('created_at',)
    list_filter = ('status',)


class CourseRegistrationAdmin(admin.ModelAdmin):
    list_display = ('student', 'course', 'registration_date', 'expiration_date', 'status')
    list_filter = ('course__status',)


admin.site.register(Course, CourseAdmin)
admin.site.register(CoursesRegistration, CourseRegistrationAdmin)
