from django.urls import path

from courses.views import RegistrationDetail

urlpatterns = [
    path('students-list/<int:pk>/', RegistrationDetail.as_view(), name='registration-students'),
]
