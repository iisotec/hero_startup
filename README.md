## Instrucciones
Se ha incluido la base de datos para ahorrar tiempo de importacion de la data desde las hojas de excel, ya que puede llegar a ser bastante tardado.

Los archivos utilizados para parsear la inforacion se encuentran en el app llamada "parsers"

| Archivo | Funcion |
| ------ | ------- |
| analytic_parser | Carga la informacion de Analitica de Videos |
| courses_parser | Carga el listado de archivos de cursos |
| registration_parsers | Carga el listado de estudiantes inscritos a los cursos |
| students_parser | Carga el listado de usuarios del proyecto |
| topic_parser | Carga el listado de temas |

### Ejecutar el servidor:
Una vez descargado el repositorio crear el ambiente de desarrollo (de preferencia)., En Linux y Mac:
```sh
python3 -m venv venv
```


Activar el entorno de desarrollo, en Mac y Linux:
```sh
source venv/bin/activate
```

Instalar las librerias necesarias para el buen funcionamiento del proyecto
```sh
pip install -r requirements.txt
```

Si se han seguido las instrucciones correctamente ejecutar
```sh
python manage.py runserver
```

Una vez el servidor se encuentra funcionando se debe mostrar en pantalla algo similar a la siguiente informacion:
```sh

System check identified no issues (0 silenced).
March 11, 2021 - 21:27:08
Django version 3.1.7, using settings 'hero_startup.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.
```

Copiar la url e ingresar con las siguientes credenciales:
```sh
Usuario: capuche777
password: clave123
```