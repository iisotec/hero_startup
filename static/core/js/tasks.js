const parseGroups = el => {
    let str_group = ''
    el.forEach((v, i, el) => {
        if (i === el.length - 1){
            str_group += `${v.name}`
        } else {
            str_group += `${v.name}, `
        }
    })
    return str_group
}

const drawOrNot = el => {
    if (el) {
        return el
    } else {
        return ''
    }
}

const yesOrNot = el => {
    if (el) {
        return  `<i class="fas fa-check-circle"></i>`
    } else {
        return `<i class="fas fa-times-circle"></i>`
    }
}

const createTable = async registers => {
    let html = ''
    registers.forEach(el => {


        html += `<tr>
            <td>${el['student']['username']}</td>
            <td>${parseGroups(el['student']['groups'])}</td>
            <td>${el['course']['name']}</td>
            <td>${drawOrNot(el['registration_date'])}</td>
            <td>${drawOrNot(el['expiration_date'])}</td>
            <td>${yesOrNot(el['course']['free'])}</td>
            <td>${yesOrNot(el['status'])}</td>
        </tr>`
    })

    document.getElementById('tableInner').innerHTML = html
    $('#dataTable').dataTable()
}

axios.get(`/api/registration/`)
    .then(
        res => createTable(res.data)
    )
