from django.urls import path

from topics.views import AnalyticsView

urlpatterns = [
    path('<int:course>/<int:student>/', AnalyticsView.as_view(), name='analytics')
]
