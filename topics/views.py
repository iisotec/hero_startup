import math

from django.contrib.auth.models import User
from django.views.generic import TemplateView

from courses.models import CoursesRegistration


class AnalyticsView(TemplateView):
    template_name = 'topics/progress_detail.html'

    def get_context_data(self, **kwargs):
        student = User.objects.get(id=kwargs['student'])
        reg = CoursesRegistration.objects.get(student=kwargs['student'], course=kwargs['course'])
        topics = reg.analytics_course.all().order_by('topic__order')
        total = reg.analytics_course.all().count()
        seen = reg.analytics_course.filter(finished=True).count()

        percent = 0

        if total > 0:
            percent = (seen * 100) / total

        context = super(AnalyticsView, self).get_context_data()
        context['title'] = student
        context['topics'] = topics
        context['percent'] = math.ceil(percent)
        context['subtitle'] = 'Contenido'

        return context
