from django.db import models
from django.db.models.fields.related import ForeignKey

from courses.models import CoursesRegistration


class Topic(models.Model):
    title = models.CharField(max_length=255, verbose_name='Título')
    short_title = models.CharField(max_length=255, verbose_name='Título corto')
    slug = models.SlugField()
    document_type = models.CharField(max_length=20, verbose_name='Tipo de documento')
    created_at = models.DateTimeField(null=True, blank=True, verbose_name='Fecha de creación')
    updated_at = models.DateTimeField(null=True, blank=True, verbose_name='Fecha de actualización')
    status = models.BooleanField(verbose_name='Estado')
    free =  models.BooleanField(verbose_name='Es Gratis')
    order = models.IntegerField(verbose_name='Ordén')

    class Meta:
        verbose_name = 'Tema'
        verbose_name_plural = 'Temas'
    
    def __str__(self):
        return self.title


class VideoAnalytics(models.Model):
    course_registration = ForeignKey(CoursesRegistration, on_delete=models.CASCADE, related_name='analytics_course', verbose_name='Inscripción')
    created_at = models.DateTimeField(null=True, blank=True, verbose_name='Fecha de creación')
    updated_at = models.DateTimeField(null=True, blank=True, verbose_name='Fecha de actualización')
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE, related_name='analytics_topic', verbose_name='Tema')
    finished = models.BooleanField(default=0, verbose_name='Completado')
    played_quantity = models.IntegerField(verbose_name='Cantidad')
    full_screen = models.BooleanField(verbose_name='Pantalla completa')

    class Meta:
        verbose_name = 'Analítica de vídeo'
        verbose_name_plural = 'Analítica de vídeos'

    def __str__(self):
        return super().__str__()
