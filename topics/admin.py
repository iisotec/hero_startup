from django.contrib import admin

from .models import Topic, VideoAnalytics


class TopicAdmin(admin.ModelAdmin):
    list_display = ('title', 'document_type', 'status', 'free', 'order')
    readonly_fields = ('created_at', 'updated_at')


class VideoAnalyticsAdmin(admin.ModelAdmin):
    # TODO: Se ha incluido solo para pruebas iniciales
    list_display = ('id', 'course_registration', 'topic', 'finished', 'played_quantity', 'full_screen')
    search_fields = ('course_registration__id',)
    list_filter = ('course_registration',)
    readonly_fields = ('created_at', 'updated_at')


admin.site.register(Topic, TopicAdmin)
admin.site.register(VideoAnalytics, VideoAnalyticsAdmin)  # Eliminar comentario para ver en admin
