from django.contrib.auth.decorators import login_required
from django.urls import path

from .views import HomePageView, TaskOneView, TaskTwoView

urlpatterns = [
    path('', HomePageView.as_view(), name='index'),
    path('task-one/', login_required(TaskOneView.as_view()), name='task-one'),
    path('task-two/', login_required(TaskTwoView.as_view()), name='task-two'),
]
