from django.urls import reverse_lazy
from django.views.generic import RedirectView, TemplateView, ListView
from courses.models import Course


class HomePageView(RedirectView):
    url = reverse_lazy('login')


class TaskOneView(TemplateView):
    template_name = 'core/tables/task-1.html'

    def get_context_data(self, **kwargs):
        context = super(TaskOneView, self).get_context_data()
        context['title'] = 'Tarea 1'
        return context


class TaskTwoView(ListView):
    template_name = 'courses/courses_list.html'
    model = Course
    context_object_name = 'courses'

    def get_context_data(self, **kwargs):
        context = super(TaskTwoView, self).get_context_data()
        context['title'] = 'Lista de cursos'
        return context
