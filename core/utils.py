import datetime
from django.utils import timezone


def parsing_dates(date_field):
    """It parse the received date and format to be accepeted in Django

    Args:
        date_field (str): The date in string field comes from an xls sheet

    Returns:
        The date to be accepted in Django or None if the date is blank
    """
    tz = timezone.get_current_timezone()
    if date_field != '':
        return tz.localize(datetime.datetime.strptime(date_field, '%Y-%m-%d %H:%M:%S'))
    else:
        return None
