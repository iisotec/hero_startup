from django.urls import path, include

from .views import UserAPI, CourseAPI, CourseRetrieveAPI, CourseRegistrationAPIList


urlpatterns = [
    path('users/', UserAPI.as_view(), name='users-list'),
    path('courses/', CourseAPI.as_view(), name='courses-list'),
    path('courses/<int:pk>/', CourseRetrieveAPI.as_view(), name='courses-detail'),
    path('registration/', CourseRegistrationAPIList.as_view(), name='registrations-list')
]
