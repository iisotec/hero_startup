from rest_framework import serializers

from django.contrib.auth.models import User, Group
from courses.models import Course, CoursesRegistration


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ('name',)


class UserSerializer(serializers.ModelSerializer):
    """Serialize list of users"""
    groups = GroupSerializer(many=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'groups')


class CourseSerializer(serializers.ModelSerializer):
    """ Course serializer """
    total_students = serializers.ReadOnlyField()

    class Meta:
        model = Course
        fields = ('id', 'name', 'free', 'total_students')


class CoursesRegistrationSerializer(serializers.ModelSerializer):
    """ Course Registration """
    student = UserSerializer()
    course = CourseSerializer()
    registration_date = serializers.DateTimeField(format='%d/%m/%Y')
    expiration_date = serializers.DateTimeField(format='%d/%m/%Y')

    class Meta:
        model = CoursesRegistration
        fields = ('id', 'student', 'course', 'registration_date', 'expiration_date', 'status')
