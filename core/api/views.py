from rest_framework import generics
from django.contrib.auth.models import User
from rest_framework.permissions import IsAuthenticatedOrReadOnly

from courses.models import Course, CoursesRegistration

from .serializers import UserSerializer, CourseSerializer, CoursesRegistrationSerializer


class UserAPI(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class CourseAPI(generics.ListAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer


class CourseRetrieveAPI(generics.RetrieveAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer


class CourseRegistrationAPIList(generics.ListAPIView):
    queryset = CoursesRegistration.objects.all().exclude(course__status=False)
    serializer_class = CoursesRegistrationSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
